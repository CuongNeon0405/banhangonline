<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','FronendController@getHome');
Route::get('category/{id}','FronendController@getcategory');
Route::get('branch/{id}','FronendController@getbranch');

Route::get('detail/{id}','FronendController@getdetail');
Route::post('detail/{id}','FronendController@postComment');
Route::get('search','FronendController@getsearch');
Route::group(['prefix'=>'cart'],function (){
   Route::get('add/{id}','CartController@getadd');
    Route::get('show','CartController@getshow');
    Route::get('detele/{id}','CartController@getdetele');
    Route::get('update','CartController@getupdate');
    Route::post('show','CartController@postCom');





});