@extends('backend.master');
@section('title',' Sửa Món Ăn ');
@section('main')

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Món Ăn Cửa  Hàng</h1>
			</div>
		</div><!--/.row-->

		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">

				<div class="panel panel-primary">
					<div class="panel-heading">Sửa Món Ăn</div>
					<div class="panel-body">
						@include('errors.note')
						<form method="post" enctype="multipart/form-data">
							<div class="row" style="margin-bottom:40px">
								<div class="col-xs-8">
									<div class="form-group" >
										<label>Tên món ăn</label>
										<input required type="text" name="name" value="{{$productlist->product_name}}" class="form-control">
									</div>
									<div class="form-group" >
										<label>Giá món ăn</label>
										<input required type="number" name="price"  value="{{$productlist->product_price}}" class="form-control">
									</div>
									<div class="form-group" >
										<label>Ảnh món ăn</label>

										<input type="file" id="img" name="img" >
										<img src="{{asset('avatar/'.$productlist->product_img)}}" height="150px">

									</div>

									<div class="form-group" >
										<label>Khuyến mãi</label>
										<input required type="text" name="promotion" value="{{$productlist->product_promotion}}" class="form-control">
									</div>
									<div class="form-group" >
										<label>Tình trạng</label>
										<input required type="text" name="condition" value="{{$productlist->product_condtion}}" class="form-control">
									</div>
									<div class="form-group" >
										<label>Trạng thái</label>
										<select required name="status" class="form-control">
											<option value="1" @if($productlist->product_status==1) checked @endif>Còn hàng</option>
											<option value="0"@if($productlist->product_status==0) checked @endif>Hết hàng</option>
										</select>
									</div>

									<div class="form-group">
										<label for="exampleFormControlTextarea2">Mô  tả</label>
										<textarea class="form-control rounded-0" name="description"  id="exampleFormControlTextarea2"  rows="3">{{$productlist->product_description}}</textarea>
									</div>
									<div class="form-group">
										<label for="exampleFormControlTextarea2">Nguyên Liệu</label>
										<textarea class="form-control rounded-0" name="ingredient" id="exampleFormControlTextarea2" rows="3">{{$productlist->product_ingredient}}</textarea>
									</div>
									<div class="form-group" >
										<label>Thể Loại Món</label>
										<select required name="product_cate" class="form-control">
											@foreach($catelist as $cate)
												<option value="{{$cate->cateid}}" @if($productlist->product_cate==$cate->cateid) selected @endif>{{$cate->name_cate}}</option>
											@endforeach

										</select>
									</div>
									<div class="form-group" >
										<label>Chí Nhánh  Bán</label>
										<select required name="prod_branch" class="form-control">
											@foreach($bralist as $bra)
												<option value="{{$bra->braid}}"  @if($productlist->prod_branch==$bra->braid) selected @endif>{{$bra->name_branch}}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group" >
										<label>Món ăn nổi bật</label><br>
										Có: <input type="radio" name="featured" value="1"@if($productlist->product_featured==1) selected @endif>
										Không: <input type="radio" checked name="featured" value="0" @if($productlist->product_featured==0) selected @endif>
									</div>
									<input type="submit" name="submit" value="Sửa" class="btn btn-primary">
									<a href="{{asset('admin/product')}}" class="btn btn-danger">Hủy bỏ</a>
								</div>
							</div>
							{{csrf_field()}}
						</form>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	</div>