@extends('backend.master');
@section('title',' Sửa Chí  Nhánh ');
@section('main')

    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Chí Nhánh  {{$branch->name_branch}}</h1>
            </div>
        </div><!--/.row-->

        <div class="row" >
            <div class="col-xs-12 col-md-5 col-lg-5">
                <div class="panel panel-primary">
                    <div class="panel-heading">

                        Sửa danh mục Món Ăn
                    </div>
                    <div class="panel-body">
                        <form method="post">
                            <div class="form-group">
                                <label>Tên chi nhánh:</label>
                                <input required type="text" name="name" class="form-control" value="{{$branch->name_branch}}" >

                            </div>
                            <div class="form-group">
                                <label>Địa Chỉ  chí  nhánh:</label>
                                <input required type="text" name="adđrec" class="form-control" value="{{$branch->addre_branch}}" >

                            </div>
                            <div class="form-group">
                                <label>số  điện thoại chi nhánh:</label>
                                <input required type="text" name="phone" class="form-control" value="{{$branch->phone}}" >

                            </div>
                            <div class="form-group">
                                <label>email chi nhánh:</label>
                                <input required type="email" name="email" class="form-control" value="{{$branch->email}}" >

                            </div>
                            <div class="form-group">
                                <input type="submit" name="subm" class="form-control btn btn-primary" value="Sửa Chí Nhánh">

                            </div>

                            <div class="form-group">
                                <a href="{{asset('admin/branch')}}" class="form-control btn btn-danger">Hủy bỏ</a>


                            </div>
                            {{csrf_field()}}
                        </form>

                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->
