<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Models\Product;
use Mail;

class CartController extends Controller
{
    public function getadd($id){
       $pro= Product::find($id);
       Cart::add(['id'=>$id,'name'=>$pro->product_name,'qty'=>1, 'price'=>$pro->product_price,'options'=>['img'=>$pro->product_img]]);
       return redirect('cart/show');

    }
    public function getshow(){
        $data['totol'] = Cart::total();
        $data['item']= Cart::content();
        return view('frontend.cart',$data);

    }
    public function getdetele($id){
        if($id=='all'){
            Cart::destroy();
        }else{
            Cart::remove($id);
        }
         return back();
    }
    public  function getupdate(Request $re){
        Cart::update($re->rowId , $re->qty);

    }

    public function postCom(Request $re)
    {

    }
}
