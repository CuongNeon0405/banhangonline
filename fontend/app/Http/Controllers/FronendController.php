<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Branch;
use App\Models\Comment;
class FronendController extends Controller
{
    public function getHome(){
        $data['featust']= Product::where('product_featured',1)->orderBy('productid','desc')->take(8)->get();
        $data['news'] = Product::orderBy('productid','desc')->take(8)->get();
        return view('frontend.home',$data);
    }
    public function getdetail($id){
        $data['item'] = Product::find($id);
        $data['coment']= Comment::where('com_pro',$id)->get();
        return view('frontend.details',$data);
    }
    public function getcategory($id){
        $data['catename'] = Category::find($id);
        $data['items']= Product::where('product_cate',$id)->orderBy('productid','desc')->paginate(8);
        return view('frontend.category',$data);

    }
    public function getbranch($id){
        $data['braname'] = Branch::find($id);
        $data['items']= Product::where('prod_branch',$id)->orderBy('productid','desc')->paginate(8);
        return view('frontend.baranch',$data);

    }
    public function postComment(Request $re, $id){
        $com  = new Comment;
        $com->com_name = $re->name;
        $com->com_email = $re->email;
        $com->com_content = $re->content;
        $com->com_pro = $id;
        $com->save();
        return back();

    }

    public function getsearch(Request $re){
        $result  = $re->text;
        $data['keyword'] = $result;
        $result = str_replace(' ','%',$result);
        $data['item']= Product::where('product_name','like' ,'%'.$result .'%')->get();
        return view('frontend.search',$data);
    }

}
