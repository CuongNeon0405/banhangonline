<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VpProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vp_product', function (Blueprint $table) {
            $table->increments('productid');
            $table->string('product_name');
            $table->string('product_img');
            $table->integer('product_price');
            $table->string('product_condtion');
            $table->string('product_promotion');
            $table->tinyInteger('product_status');
            $table->text('product_description');
            $table->text('product_ingredient');
            $table->tinyInteger('product_featured');
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
            $table->integer('product_cate')->unsigned();
            $table->integer('prod_branch')->unsigned();
            $table->foreign('product_cate')->references('cateid')->on('vp_category');
            $table->foreign('prod_branch')->references('braid')->on('vp_branch');








          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vp_product');
    }
}
