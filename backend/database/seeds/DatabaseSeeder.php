<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $data = [
            'email' =>'cuong@gmail.com',
            'password'=> bcrypt('cuong12'),
            'level'=>1
        ];
        DB::table('vp_user')->insert($data);
    }
}
