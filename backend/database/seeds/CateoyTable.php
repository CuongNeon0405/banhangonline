<?php

use Illuminate\Database\Seeder;

class CateoyTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name_cate' =>'đồ ăn nhanh',
                'cate_slug'=> str_slug('doannhanh')
            ],
            [
                'name_cate' =>'giải khát',
                'cate_slug'=> str_slug('giaikhat')
            ],

        ];
        DB::table('vp_category')->insert($data);
    }
}
