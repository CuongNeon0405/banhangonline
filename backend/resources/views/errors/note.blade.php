@if(Session::has('error'))
    <p class="alert alert-primary">{{Session::get('error')}}</p>
    @endif