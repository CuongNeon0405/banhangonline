@extends('backend.master');
@section('title','Chi Nhánh Cửa hàng');
@section('main')

    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Chi Nhánh Cửa hàng</h1>
            </div>
        </div><!--/.row-->

        <div class="row">
            <div class="col-xs-12 col-md-5 col-lg-5">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Mở Chi Nhánh
                    </div>
                    @include('errors.note')

                    <div class="panel-body">
                        <form method="post">
                            <div class="form-group">
                                <label>Tên chi nhánh:</label>
                                <input required type="text" name="name" class="form-control" >

                            </div>
                            <div class="form-group">
                                <label>Địa Chỉ  chí  nhánh:</label>
                                <input required type="text" name="adđrec" class="form-control" >

                            </div>
                            <div class="form-group">
                                <label>số  điện thoại chi nhánh:</label>
                                <input required type="text" name="phone" class="form-control" >

                            </div>
                            <div class="form-group">
                                <label>email chi nhánh:</label>
                                <input required type="email" name="email" class="form-control" >

                            </div>
                            <div class="form-group">
                                <input type="submit" name="subm" class="form-control btn btn-primary" value="Mở Chí Nhánh">

                            </div>

                            <div class="form-group">
                                <input type="submit" name="huysubmit" class="form-control btn btn-warning" value="Hủy Bỏ">

                            </div>
                            {{csrf_field()}}
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-7 col-lg-7">
                <div class="panel panel-primary">
                    <div class="panel-heading">Chi Nhánh Cửa hàng</div>
                    <div class="panel-body">
                        <div class="bootstrap-table">
                            <table class="table table-bordered">
                                <thead>
                                <tr class="bg-primary">
                                    <th>Tên chí Nhánh</th>
                                    <th>địa chỉ  chi nhánh</th>
                                    <th>Phone</th>
                                    <th>email</th>


                                    <th style="width:30%">Tùy chọn</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($branch as $bra )
                                    <tr>
                                        <td>{{$bra->name_branch}}</td>
                                        <td>{{$bra->addre_branch}}</td>
                                        <td>{{$bra->phone}}</td>
                                        <td>{{$bra->email}}</td>



                                        <td>
                                            <a href="{{asset('admin/branch/edit/'.$bra->braid)}}" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Sửa</a>
                                            <a href="{{asset('admin/branch/detlete/'.$bra->braid)}}" onclick="return confirm('Bạn có chắc chắn đóng chí nhánh?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $branch->links() }}

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->
@stop