@extends('backend.master');
@section('title','Loại Món');
@section('main')
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Danh mục Món Ăn</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-xs-12 col-md-5 col-lg-5">
					<div class="panel panel-primary">
						<div class="panel-heading">
							Thêm danh mục
						</div>
						@include('errors.note')

						<div class="panel-body">
							<form method="post">
								<div class="form-group">
									<label>Tên danh mục:</label>
									<input required type="text" name="name" class="form-control" >

								</div>
								<div class="form-group">
									<input type="submit" name="subm" class="form-control btn btn-primary" value="Thêm Danh Mục Món Ăn">

								</div>

								<div class="form-group">
									<input type="submit" name="huysubmit" class="form-control btn btn-warning" value="Hủy Bỏ">

								</div>
								{{csrf_field()}}
							</form>

						</div>
					</div>
			</div>
			<div class="col-xs-12 col-md-7 col-lg-7">
				<div class="panel panel-primary">
					<div class="panel-heading">Danh sách danh mục món ăn</div>
					<div class="panel-body">
						<div class="bootstrap-table">
							<table class="table table-bordered">
				              	<thead>
					                <tr class="bg-primary">
					                  <th>Tên danh mục</th>
					                  <th style="width:30%">Tùy chọn</th>
					                </tr>
				              	</thead>
				              	<tbody>
								@foreach($catelist as $cate )
								<tr>
									<td>{{$cate->name_cate}}</td>
									<td>
			                    		<a href="{{asset('admin/category/edit/'.$cate->cateid)}}" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Sửa</a>
			                    		<a href="{{asset('admin/category/detlete/'.$cate->cateid)}}" onclick="return confirm('Bạn có chắc chắn muốn xóa?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
			                  		</td>
			                  	</tr>
                                     @endforeach
				                </tbody>
				            </table>
							{{ $catelist->links() }}

						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	</div>	<!--/.main-->
@stop