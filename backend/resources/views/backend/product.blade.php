@extends('backend.master');
@section('title','Danh sách  Món ăn');
@section('main')
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Món Ăn Cửa Hàng</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">
				
				<div class="panel panel-primary">
					<div class="panel-heading">Danh sách Món Ăn</div>
					<div class="panel-body">
						<div class="bootstrap-table">
							<div class="table-responsive">
								<a href="{{asset('admin/product/add')}}" class="btn btn-primary">Thêm sản phẩm</a>
								<table class="table table-bordered" style="margin-top:20px;">				
									<thead>
										<tr class="bg-primary">
											<th width="10%">Tên Món</th>
											<th width="10%">Giá sản phẩm</th>
											<th width="10%">Ảnh sản phẩm</th>
											<th>Loại Món</th>
											<th>Tình Trạng</th>
                                            <th width="20%">Nguyên Liệu</th>

                                            <th>Chí  Nhánh bán</th>
											<th>Tùy chọn</th>
										</tr>
									</thead>
									<tbody>
									@foreach($productlist as $product)
										<tr>
											<td> {{$product->product_name}}</td>
											<td> {{number_format($product->product_price,0,',','.')}}VND</td>
											<td>
												<img src="{{asset('avatar/'.$product->product_img)}}" height="150px">
											</td>
											<td>{{$product->name_cate}}</td>
											<td>{{$product->product_condtion}}</td>

											<td>{{$product->product_ingredient}}</td>
											<td>{{$product->name_branch}}</td>
											<td>
												<a href="{{asset('admin/product/edit/'.$product->productid)}}" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Sửa</a>
												<a href="{{asset('admin/product/detlete/'.$product->productid)}}" onclick="return confirm('Bạn có chắc chắn muốn xóa món ăn này?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>							
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
