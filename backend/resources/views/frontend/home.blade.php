@extends('frontend.master')
@section('title','Home')
@section('main')

					<div id="wrap-inner">
						<div class="products">
							<h3>sản phẩm nổi bật</h3>
							<div class="product-list row">
								@foreach($featust as $pro)
								<div class="product-item col-md-3 col-sm-6 col-xs-12">
									<a href="#"><img src="{{asset('avatar/'.$pro->product_img)}}"  class="img-thumbnail"></a>
									<p><a href="#">{{$pro->product_name}}</a></p>
									<p class="price">{{number_format($pro->product_price,0,',','.')}}VND</p>
									<div class="marsk">
										<a href="{{asset('detail/'.$pro->productid)}}">Xem chi tiết</a>
									</div>
								</div>
								@endforeach

							</div>                	                	
						</div>

						<div class="products">
							<h3>sản phẩm mới</h3>
							<div class="product-list row">
								@foreach($news  as $pro)
								<div class="product-item col-md-3 col-sm-6 col-xs-12">
									<a href="#"><img src="{{asset('avatar/'.$pro->product_img)}}"  class="img-thumbnail"></a>
									<p><a href="#">{{$pro->product_name}}</a></p>
									<p class="price">{{number_format($pro->product_price,0,',','.')}}VND</p>
									<div class="marsk">
										<a href="{{asset('detail/'.$pro->productid)}}">Xem chi tiết</a>
									</div>                      	                        
								</div>
							    @endforeach
							</div>    
						</div>
					</div>
					@stop

					<!-- end main -->
