<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table  = 'vp_category';
    protected $primaryKey = 'cateid';
//    protected $guarded =[];

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
}
