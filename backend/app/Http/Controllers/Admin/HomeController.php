<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB ;
 use App\Models\Product;
use App\Models\Branch;

use App\Models\Category;


class HomeController extends Controller
{
    public function getHome(){
        $sl  =  Product::count();
        $lm  =  Category::count();
        $cc =  Branch::count();
      // dd($cc);

        //dd($sl);
        return view('backend.index' ,compact('sl' ) , compact('lm'), compact('cc') );
    }
    public function logout(){
        Auth::logout();
       return redirect()->intended('login');
    }
}
