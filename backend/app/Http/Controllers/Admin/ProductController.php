<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests\AddProductRequest;
use App\Models\Category;
use App\Models\Branch;
use DB;


class ProductController extends Controller
{
    public  function getproduct(){
        $data['productlist'] =  DB::table('vp_product')->join('vp_category','vp_product.product_cate','=','vp_category.cateid')->join('vp_branch','vp_product.prod_branch','=','vp_branch.braid')->orderBy('productid','desc')->get();


        return view('backend.product' , $data);
    }
    public  function getaddproduct(){
        $data['catelist'] = Category::all();
        $data['bralist'] = Branch::all();
       return view('backend.addproduct' , $data);
    }
    public  function postaddproduct(AddProductRequest $re){
        $filename  = $re->img->getClientOriginalName();
       $product = new  Product;
       $product->product_name = $re->name;
       $product->product_img = $filename;
       $product->product_price = $re->price;
       $product->product_condtion = $re->condition;
       $product->product_promotion =$re->promotion;
       $product->product_status = $re->status;
       $product->product_description = $re->description;
       $product->product_ingredient = $re->ingredient;
       $product->product_featured = $re->featured;
       $product->product_cate= $re->product_cate;
       $product->prod_branch = $re->prod_branch;
       $product->save();
       $re->img->move(public_path('avatar'),$filename);
       return back();
    }
    public  function getEditProduct($id){
        $data['productlist'] = Product::find($id);
        $data['catelist'] = Category::all();
        $data['bralist'] = Branch::all();
     return view('backend.editproduct',$data);
    }
    public  function postEditProduct(Request $re , $id){
       $pro = new Product;
        $arr['product_name'] =$re->name;
        $arr['product_price'] =  $re->price;
        $arr['product_condtion'] =  $re->condition;
        $arr['product_promotion'] =  $re->promotion;
        $arr['product_status'] = $re->status;
        $arr['product_description'] =  $re->description;
        $arr['product_ingredient'] =$re->ingredient;
        $arr['product_featured'] =$re->featured;
        $arr['product_cate'] = $re->product_cate;
        $arr['prod_branch'] =$re->prod_branch;
        if($re->hasFile('img')){
            $img  = $re->img->getClientOriginalName();
            $arr['product_img'] = $img;
            $re->img->move(public_path('avatar'),$img);
        }
        $pro::where('productid' ,$id)->update($arr);

      return redirect('admin/product');


    }
    public  function detleteProduct($id){
        Product::destroy($id);
        return back();
    }
}
