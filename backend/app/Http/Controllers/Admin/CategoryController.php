<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Requests\AddCateRequest;
use App\Http\Requests\AddCateRequesr;
use DB;


class CategoryController extends Controller
{
    public  function getCategony(){
        //$data['catelist'] = Category::all();
        $catelist= DB::table('vp_category')->paginate(3);
     return view('backend.category',compact('catelist'));
    }
    public  function postCategony(Request $re){
//        $this->validate($re, [
//            'name'=>'unique : vp_category , name_cate'
//
//        ], [
//            'name.unique'=>' loại Món này đã có '
//        ]);

        $cate = new Category();
        $cate -> name_cate = $re->name;
        $cate->save();
        return back();
    }
    public function getEditCategony($id){
        $data['cate'] =  Category::find($id);
        return view('backend.editcategory',$data);

    }
    public  function postEditCategony(Request $re , $id){

        $cate = Category::find($id);
        $cate -> name_cate = $re->name;
        $cate->save();
        return redirect()->intended('admin/category');
    }
    public function detleteCategony($id){
        Category::destroy($id);
        return back();

    }
}
