<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class loginController extends Controller
{
    public  function  getLogin(){
        return view('backend.login');
     }
     public function postLogin(Request $re){
        $arr = ['email'=>$re->email  ,'password'=>$re->password];

        if($re->remember = 'checkbox'){
            $remember = true;
        }else{
            $remember =  false;
        }
        if(Auth::attempt($arr,$remember)){
            return redirect()->intended('admin/home');
        }else{
            return back()->withInput()->with('error','không  phải  quản  trị viên');
        }

     }
}
