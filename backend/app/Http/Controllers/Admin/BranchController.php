<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Branch;
use DB;

class BranchController extends Controller
{
    public function getbranch()
    {
//        $data['branch'] =  Branch::all();
//        return view('backend.chinhanh',$data);
        $branch= DB::table('vp_branch')->paginate(3);
        return view('backend.chinhanh',compact('branch'));
    }
    public function  postbranch(Request $re){
        $cate = new Branch();
        $cate -> name_branch = $re->name;
        $cate -> addre_branch = $re->adđrec;
        $cate -> phone = $re->phone;
        $cate -> email = $re->email;

        $cate->save();
        return back();
    }
    public function getEditbranch($id){
        $data['branch'] =  Branch::find($id);
        return view('backend.editbaranch',$data);

    }
    public function postEditbranch(Request $re , $id){
        $branch = Branch::find($id);
        $branch -> name_branch = $re->name;
        $branch -> addre_branch = $re->adđrec;
        $branch -> phone = $re->phone;
        $branch -> email = $re->email;

        $branch->save();
        return redirect()->intended('admin/branch');
    }
}
