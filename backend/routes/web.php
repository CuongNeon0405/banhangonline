<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','FronendController@getHome');
Route::get('category/{id}','FronendController@getcategory');
Route::get('branch/{id}','FronendController@getbranch');

Route::get('detail/{id}','FronendController@getdetail');
Route::post('detail/{id}','FronendController@postComment');
Route::get('search','FronendController@getsearch');





Route::group(['namespace'=>'Admin'],function(){
    Route::group(['prefix'=>'login' ,'middleware'=>'CheckLogin'],function (){
       Route::get('/','LoginController@getLogin');
        Route::post('/','LoginController@postLogin')->name('login');

    });
    Route::get('logout','HomeController@logout');


    Route::group(['prefix'=>'admin','middleware'=>'CheckOut'],function (){
        Route::get('home','HomeController@getHome');
        Route::group(['prefix'=>'category'],function (){
            Route::get('/','CategoryController@getCategony');
            Route::post('/','CategoryController@postCategony');

            Route::get('edit/{id}','CategoryController@getEditCategony');
            Route::post('edit/{id}','CategoryController@postEditCategony');

            Route::get('detlete/{id}','CategoryController@detleteCategony');

        });
        Route::group(['prefix'=>'product'],function (){
            Route::get('/','ProductController@getproduct');
            Route::get('add','ProductController@getaddproduct');

            Route::post('add','ProductController@postaddproduct');

            Route::get('edit/{id}','ProductController@getEditProduct');
            Route::post('edit/{id}','ProductController@postEditProduct');

            Route::get('detlete/{id}','ProductController@detleteProduct');

        });
        Route::group(['prefix'=>'branch'],function (){
            Route::get('/','BranchController@getbranch');
            Route::post('/','BranchController@postbranch');

            Route::get('edit/{id}','BranchController@getEditbranch');
            Route::post('edit/{id}','BranchController@postEditbranch');

            Route::get('detlete/{id}','BranchController@detletebranch');

        });
    });
});